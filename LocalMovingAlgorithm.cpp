#include <iostream>
#include "LocalMovingAlgorithm.hpp"

#include "networkit/structures/Partition.hpp"
#include "networkit/graph/Graph.hpp"
#include "networkit/Globals.hpp"
#include "CommunitySubGraph.hpp"

using namespace NetworKit;

namespace NetworKit {

    LocalMovingAlgorithm::LocalMovingAlgorithm(const Graph &graph, Partition &zeta, double gamma) :
            totalEdgeWeight(graph.totalEdgeWeight()),
            divisor(2 * totalEdgeWeight * totalEdgeWeight),
            gamma(gamma), z(graph.upperNodeIdBound()),
            o(zeta.upperBound()),
            nodeVolumes(z, 0.0),
            communityVolumes(z, 0.0),
            neighboringCommunities(),
            turboAffinity(zeta.upperBound()) {
        this->graph = &graph;
        this->m_zeta = &zeta;

        this->graph->parallelForNodes([&](node u) {
            nodeVolumes[u] += this->graph->weightedDegree(u);
            nodeVolumes[u] += this->graph->weight(u, u);
        });

        this->m_zeta->parallelForEntries([&](node u, index community) {
            if (community != none) {
                communityVolumes[community] += nodeVolumes[u];
            }
        });
    }

    void LocalMovingAlgorithm::run() {
        std::cout << "Local Moving" << std::endl;

        bool update = false;

        auto &zeta = *m_zeta;

        // Initialize node queue
        std::queue<node> nodeQueue;
        this->graph->forNodesInRandomOrder([&](node u) {
            nodeQueue.push(u);
        });
        /*this->graph->forNodesInRandomOrder([&](node u) {
           nodeQueue.push(u);
        });*/

        std::vector<double> edgeWeightPerCluster(z);
        std::vector<count> nodesPerCluster(z);
        this->graph->forNodes([&](node u) {
            nodesPerCluster[zeta[u]] += 1;
        });

        int64_t nUnused = 0;
        std::vector<index> unusedClusters(z);
        this->graph->forNodes([&](node u) {
            if (nodesPerCluster[zeta[u]] == 0) {
                unusedClusters[nUnused] = zeta[u];
                nUnused += 1;
            }
        });

        std::vector<edgeweight> affinity(o);
        std::vector<index> neighbors(o);

        auto volCommunityMinusNode = [&](index C, node u) {
            auto communityVolume = communityVolumes[C];
            if (zeta[u] == C) {
                return communityVolume - nodeVolumes[u];
            } else {
                return communityVolume;
            }
        };

        auto modularityGain = [&](node u, index C, index D, edgeweight affinityC, edgeweight affinityD) {
            double nodeVolume = nodeVolumes[u];
            double delta = (affinityD - affinityC) / totalEdgeWeight +
                           gamma * ((volCommunityMinusNode(C, u) - volCommunityMinusNode(D, u)) * nodeVolume) / divisor;
            return delta;
        };

        std::vector<node> activeNodes(z);
        count numActive = z;

        do {
            // Get next node and its current community
            auto &u = nodeQueue.front();
            auto C = zeta[u];

            // Affinity
            neighbors.clear();
            graph->forNeighborsOf(u, [&](node v) {
                index D = zeta[v];
                affinity[D] = -1;
            });
            affinity[C] = 0;
            graph->forNeighborsOf(u, [&](node v, edgeweight ew) {
                if (u != v) {
                    index D = zeta[v];
                    if (affinity[D] == -1) {
                        affinity[D] = 0;
                        neighbors.push_back(D);
                    }
                    affinity[D] += ew;
                }
            });

            // Remove the node from its community and update the weights and counts
            communityVolumes[C] -= nodeVolumes[u];
            nodesPerCluster[C] -= 1;

            // If there are no nodes left in the community mark it as unused
            if (nodesPerCluster[C] == 0) {
                unusedClusters[nUnused] = C;
                nUnused += 1;
            }

            // Add an empty cluster to the neighborhood
            neighbors.push_back(unusedClusters[nUnused - 1]);

            auto bestCluster = C;

            // Calculate modularity gain if the node was moved back to its old cluster
            auto maxModularityGain = 0;

            for (auto D: neighbors) {
                auto delta = modularityGain(u, C, D, affinity[C], affinity[D]);

                if (delta > 0) {
                    std::cout << "" << std::endl;
                }

                // Move the node to its neighbor community if that increase the modularity score
                if (delta > maxModularityGain) {
                    bestCluster = D;
                    maxModularityGain = delta;
                }

                affinity[D] = 0;
            }

            // Move node to its new cluster
            communityVolumes[bestCluster] += nodeVolumes[u];
            zeta[u] = bestCluster;
            nodesPerCluster[bestCluster] += 1;

            // Node has been moved back into its old (currently empty) community
            if (bestCluster == unusedClusters[nUnused - 1]) {
                nUnused -= 1;
            }

            activeNodes[u] = true;
            numActive -= 1;

            // Node has been moved to a new community
            if (bestCluster != C) {
                graph->forNeighborsOf(u, [&](node v) {
                    // Visit all neighbors that are not in the same community
                    if (activeNodes[v] && zeta[v] != bestCluster) {
                        activeNodes[v] = false;
                        numActive += 1;
                        nodeQueue.push(v);
                    }
                });

                update = true;
            }

            nodeQueue.pop();
        } while (!nodeQueue.empty());

        if (update) {
            zeta.compact();
        }
    }

}