#include <iostream>
#include <random>
#include "LocalMergingAlgorithm.hpp"
#include "networkit/Globals.hpp"
#include "networkit/graph/Graph.hpp"
#include "networkit/structures/Partition.hpp"
#include <networkit/auxiliary/SignalHandling.hpp>

namespace NetworKit {

    LocalMergingAlgorithm::LocalMergingAlgorithm(const Graph &graph, double gamma, double randomness) : graph(graph), gamma(gamma), randomness(randomness) {}

    Partition LocalMergingAlgorithm::findSubGraphPartition(CommunitySubGraph subGraph) {
        std::cout << "Local Merging" << std::endl;

        count z = subGraph.graph.upperNodeIdBound();

        // Start with a singleton partition
        Partition zetaSubGraph(z);
        zetaSubGraph.allToSingletons();
        index o = zetaSubGraph.upperBound();

        // We only want to move nodes that are in singleton clusters
        std::vector<bool> isClusterNonSingleton(o, false);

        edgeweight totalEdgeWeight = subGraph.graph.totalEdgeWeight();
        auto divisor = 2 * totalEdgeWeight * totalEdgeWeight;

        edgeweight fullGraphTotalEdgeWeight = graph.totalEdgeWeight();

        // Accumulate volumes for all nodes in the subgraph
        std::vector<double> nodeVolumes(z, 0.0);
        subGraph.graph.parallelForNodes([&](node u) {
            nodeVolumes[u] += subGraph.graph.weightedDegree(u);
            nodeVolumes[u] += subGraph.graph.weight(u, u);
        });

        // We always start with a singleton partition so nodeVolumes = communityVolumes
        std::vector<double> communityVolumes(nodeVolumes);

        if (subGraph.graph.numberOfNodes() == 1) {
            return zetaSubGraph;
        }

        // The number of external edges (no self loops) per node
        std::vector<edgeweight> affinity(o);
        std::vector<index> neighbors(o);

        // The quality increase achieved by moving a node to a respective cluster
        std::vector<double> qualityIncrementPerCluster(o);

        bool update = false;

        auto volCommunityMinusNode = [&](index C, node u) {
            auto communityVolume = communityVolumes[C];
            if (zetaSubGraph[u] == C) {
                return communityVolume - nodeVolumes[u];
            } else {
                return communityVolume;
            }
        };

        auto modularityGain = [&](node u, index C, index D, edgeweight affinityC, edgeweight affinityD) {
            double nodeVolume = nodeVolumes[u];
            double delta = (affinityD - affinityC) / totalEdgeWeight +
                           gamma * ((volCommunityMinusNode(C, u) - volCommunityMinusNode(D, u)) * nodeVolume) / divisor;
            return delta;
        };

        std::vector<edgeweight> cuts(z, 0.0);
        subGraph.graph.forNodes([&](node u) {
            subGraph.graph.forNeighborsOf(u, [&](node v, edgeweight ew) {
                if (u != v) {
                    cuts[u] += ew;
                }
            });
        });

        // Visit all nodes in the subgraph in random order...
        // forNodesInRandomOrder
        subGraph.graph.forNodesInRandomOrder([&](node u) {
            index C = zetaSubGraph[u];

            // Affinity
            neighbors.clear();
            subGraph.graph.forNeighborsOf(u, [&](node v) {
                index D = zetaSubGraph[v];
                affinity[D] = -1;
            });
            // TODO: this might be wrong as it implies that C has already been moved from its community
            affinity[C] = 0.0;
            // We want to be able to move the node back to its original cluster
            neighbors.push_back(C);
            subGraph.graph.forNeighborsOf(u, [&](node v, edgeweight ew) {
                if (u != v) {
                    index D = zetaSubGraph[v];
                    if (affinity[D] == -1) {
                        affinity[D] = 0;
                        neighbors.push_back(D);
                    }
                    affinity[D] += ew;
                }
            });

            // ...but only consider nodes in singleton communities as they have not been moved yet
            if (!isClusterNonSingleton[C] &&
                (affinity[C] >= nodeVolumes[u] * (totalEdgeWeight - nodeVolumes[u]) / fullGraphTotalEdgeWeight)) {
                // Remove node from its cluster
                communityVolumes[C] = 0;
                affinity[C] = 0;

                index bestCluster = C;
                double maxQualityIncrement = 0;
                double totalQualityIncrement = 0;

                index neighborIndex = 0;
                // Find the neighbor community that yields the highest modularity gain
                for (auto D: neighbors) {
                    if (affinity[D] >=
                        communityVolumes[D] * (totalEdgeWeight - communityVolumes[D]) / fullGraphTotalEdgeWeight) {
                        auto delta = modularityGain(u, C, D, affinity[C], affinity[D]);

                        if (delta > maxQualityIncrement) {
                            bestCluster = D;
                            maxQualityIncrement = delta;
                        }

                        if (delta >= 0) {
                            totalQualityIncrement += std::exp(delta / randomness);
                        }
                    }

                    // Keep track of the quality gains in a prefix sum kind of way
                    qualityIncrementPerCluster[neighborIndex] = totalQualityIncrement;
                    affinity[D] = 0;
                    neighborIndex += 1;
                }

                index chosenCluster = bestCluster;

                if (totalQualityIncrement < std::numeric_limits<double>::max()) {
                    // Start at a random "position" between 0 and the sum of quality increments
                    auto r = totalQualityIncrement * Aux::Random::real();

                    int64_t min_idx = -1;
                    int64_t max_idx = neighbors.size() + 1;

                    // Linear search to find the cluster with a quality increment larger than r
                    while (min_idx < max_idx - 1) {
                        auto mid_idx = (min_idx + max_idx) / 2;
                        if (qualityIncrementPerCluster[mid_idx] >= r) {
                            max_idx = mid_idx;
                        } else {
                            min_idx = mid_idx;
                        }
                    }

                    chosenCluster = neighbors[max_idx];
                }

                // Move node to its new community
                communityVolumes[chosenCluster] += nodeVolumes[u];

                subGraph.graph.forNeighborsOf(u, [&](node v) {
                    if (zetaSubGraph[v] == chosenCluster) {
                        communityVolumes[chosenCluster] -= nodeVolumes[v];
                    } else {
                        communityVolumes[chosenCluster] += nodeVolumes[v];
                    }
                });

                if (chosenCluster != u) {
                    zetaSubGraph[u] = chosenCluster;
                    isClusterNonSingleton[chosenCluster] = true;
                    update = true;
                }
            }
        });

        if (update) {
            zetaSubGraph.compact();
        }

        return zetaSubGraph;
    }

}