//
// Created by rerz on 09.02.21.
//

#ifndef LEIDEN_COMMUNITYSUBGRAPH_HPP
#define LEIDEN_COMMUNITYSUBGRAPH_HPP

#include "networkit/graph/Graph.hpp"

using namespace NetworKit;

namespace NetworKit {

    class CommunitySubGraph {
    public:
        CommunitySubGraph(Graph graph, std::map<node, node> subGraphToGraphMapping,
                          std::map<node, node> graphToSubGraphMapping);

        Graph graph;
        std::map<node, node> subGraphToGraphMapping;
        std::map<node, node> graphToSubGraphMapping;
    };

}

#endif //LEIDEN_COMMUNITYSUBGRAPH_HPP
