#include <iostream>
#include <networkit/graph/Graph.hpp>
#include <networkit/generators/ErdosRenyiGenerator.hpp>
#include <networkit/generators/ClusteredRandomGraphGenerator.hpp>
#include <networkit/auxiliary/Log.hpp>
#include <networkit/auxiliary/SignalHandling.hpp>
#include <networkit/auxiliary/Timer.hpp>
#include <networkit/community/PLM.hpp>
#include <networkit/coarsening/ClusteringProjector.hpp>
#include <networkit/coarsening/ParallelPartitionCoarsening.hpp>
#include <networkit/graph/GraphBuilder.hpp>
#include "LeidenAlgorithm.hpp"
#include <networkit/io/EdgeListReader.hpp>
#include <networkit/io/EdgeListWriter.hpp>
#include <networkit/io/MatrixMarketReader.hpp>
#include <networkit/io/DotPartitionWriter.hpp>
#include <fstream>

using namespace NetworKit;

int main() {
    ClusteredRandomGraphGenerator generator(30, 3, 0.9, 0.1);

    auto graph = generator.generate();

    std::cout << "Num Edges: " << graph.numberOfEdges() << std::endl;

    auto communities = generator.getCommunities();

    std::ofstream currentGraph("currentGraph.txt");

    EdgeListWriter elWriter('\t', 0, false);

    if (!currentGraph.is_open()) {
        std::cerr << "oops" << std::endl;
    }

    graph.forEdges([&](node u, node v, edgeweight ew) {
        currentGraph << u << " " << v << " " << ew << std::endl;
    });

    elWriter.write(graph, "graph.el");

    currentGraph.close();

    DotPartitionWriter writer;

    LeidenAlgorithm leiden(graph);
    leiden.run();

    auto finalPartition = leiden.getPartition();

    writer.write(const_cast<Graph &>(leiden.getGraph()), finalPartition, "partition.dot");

    return 0;
}