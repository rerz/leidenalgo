#ifndef LEIDEN_LOCALMERGINGALGORITHM_HPP
#define LEIDEN_LOCALMERGINGALGORITHM_HPP
#include "networkit/structures/Partition.hpp"
#include "CommunitySubGraph.hpp"

namespace NetworKit {

    class LocalMergingAlgorithm {
    public:
        LocalMergingAlgorithm(const Graph &graph, double gamma, double randomness);

        Partition findSubGraphPartition(CommunitySubGraph subGraph);

    private:
        const Graph &graph;
        double gamma;
        double randomness;
    };

}

#endif //LEIDEN_LOCALMERGINGALGORITHM_HPP
