#include <networkit/graph/GraphBuilder.hpp>
#include <networkit/auxiliary/SignalHandling.hpp>
#include <iostream>
#include <networkit/coarsening/ParallelPartitionCoarsening.hpp>
#include "LeidenAlgorithm.hpp"
#include "LocalMovingAlgorithm.hpp"
#include "LocalMergingAlgorithm.hpp"
#include <networkit/community/CommunityDetectionAlgorithm.hpp>

namespace NetworKit {
    LeidenAlgorithm::LeidenAlgorithm(const Graph &graph) :
            CommunityDetectionAlgorithm(graph),
            G(&graph),
            zeta(Partition(graph.upperNodeIdBound())) {
        this->zeta.allToSingletons();
    }

    LeidenAlgorithm::LeidenAlgorithm(const Graph &graph, Partition &initialPartition) :
            CommunityDetectionAlgorithm(graph, initialPartition),
            G(&graph),
            zeta(initialPartition) {
    }

    std::vector<CommunitySubGraph> LeidenAlgorithm::getCommunitySubGraphs() {
        auto communities = this->zeta.getSubsets();

        std::vector<CommunitySubGraph> subGraphs;

        for (auto &nodes: communities) {
            GraphBuilder graphBuilder(nodes.size(), true, false);

            std::map<node, node> subGraphToGraphMapping;
            std::map<node, node> graphToSubGraphMapping;

            node nodeIndex = 0;
            for (node u: nodes) {
                subGraphToGraphMapping.insert(std::make_pair(nodeIndex, u));
                graphToSubGraphMapping.insert(std::make_pair(u, nodeIndex));

                nodeIndex += 1;
            }

            for (auto u: nodes) {
                G->forEdgesOf(u, [&](node v, edgeweight ew) {
                    if (zeta[u] == zeta[v]) {
                        graphBuilder.addHalfEdge(graphToSubGraphMapping[u], graphToSubGraphMapping[v], ew);
                    }
                });
            }

            auto graph = graphBuilder.toGraph(false);

            CommunitySubGraph subGraph(graph, subGraphToGraphMapping, graphToSubGraphMapping);
            subGraphs.push_back(subGraph);
        }

        return subGraphs;
    }

    void LeidenAlgorithm::run() {
        Aux::SignalHandler handler;

        handler.assureRunning();

        count z = this->G->upperNodeIdBound();
        index o = this->zeta.upperBound();
        std::vector<double> nodeVolumes(z, 0.0);

        // Perform local moving
        LocalMovingAlgorithm localMoving(*this->G, this->zeta, 1.0);
        localMoving.run();

        // If all nodes are in a singleton partition we are done
        if (zeta.numberOfSubsets() < G->upperNodeIdBound()) {
            auto subGraphs = getCommunitySubGraphs();

            LocalMergingAlgorithm localMerging(*this->G, 1.0, 1e-2);

            // The refinement can have at most as many communities as are nodes in the graph
            Partition refinement(z);
            refinement.setUpperBound(z);

            index communityCount = 0;
            for (const auto &subGraph: subGraphs) {
                // Run local merging on subgraph induced by the partition
                Partition subGraphPartition = localMerging.findSubGraphPartition(subGraph);

                // Iterate through the detected communities within the subgraph...
                for (auto &cluster: subGraphPartition.getSubsets()) {
                    // ...and update the nodes' communities in the main graph
                    for (auto &node: cluster) {
                        auto nodeInMain = subGraph.subGraphToGraphMapping.at(node);
                        refinement[nodeInMain] = communityCount;
                    }
                    communityCount += 1;
                }
            }

            std::pair<Graph, std::vector<node>> coarsened;
            Partition zetaCoarse;

            if (refinement.numberOfSubsets() < G->upperNodeIdBound()) {
                // Create coarsened graph based on the refinement
                ParallelPartitionCoarsening parCoarsening(*G, refinement);
                parCoarsening.run();

                auto coarse = parCoarsening.getCoarseGraph();

                zetaCoarse = Partition(refinement.numberOfSubsets());
                zetaCoarse.setUpperBound(refinement.numberOfSubsets());

                G->forNodes([&](node u) {
                    // Each node in the coarse graph corresponds to one community
                    zetaCoarse[refinement[u]] = zeta[u];
                });

                coarsened = {parCoarsening.getCoarseGraph(), parCoarsening.getFineToCoarseNodeMapping()};
            } else {
                // Create coarsened graph based on the original partition (we don't want to coarsen singleton partitions as nothing would change)
                ParallelPartitionCoarsening parCoarsening(*G, zeta);
                parCoarsening.run();

                coarsened = {parCoarsening.getCoarseGraph(), parCoarsening.getFineToCoarseNodeMapping()};

                zetaCoarse = Partition(coarsened.first.upperNodeIdBound());
                zetaCoarse.setUpperBound(coarsened.first.upperNodeIdBound());
            }

            Graph coarseGraph = coarsened.first;

            // Run the same steps on the coarsened graph
            LeidenAlgorithm onCoarse(coarseGraph, zetaCoarse);
            onCoarse.run();

            Partition zetaFine(z);
            zetaFine.setUpperBound(zetaCoarse.upperBound());

            // Map the meta node communities back onto the normal nodes
            G->forNodes([&](node v) {
                node mv = coarsened.second[v];
                index cv = zetaCoarse[mv];
                zetaFine[v] = cv;
            });

            zeta = zetaFine;
        }
    }

    Partition LeidenAlgorithm::getPartition() {
        return this->zeta;
    }

    const Graph &LeidenAlgorithm::getGraph() {
        return *this->G;
    }

}