#ifndef LEIDEN_LOCALMOVINGALGORITHM_HPP
#define LEIDEN_LOCALMOVINGALGORITHM_HPP

#include "networkit/structures/Partition.hpp"
#include "networkit/graph/Graph.hpp"
#include "networkit/Globals.hpp"
#include "CommunitySubGraph.hpp"

using namespace NetworKit;

namespace NetworKit {

    class LocalMovingAlgorithm {
    public:
        LocalMovingAlgorithm(const Graph &graph, Partition &zeta, double gamma);

        void run();

    private:
        const Graph *graph;
        Partition *m_zeta;

        count z;
        index o;

        bool moved = false;
        bool changed = false;

        double gamma;
        edgeweight totalEdgeWeight;
        edgeweight divisor;

        std::vector<edgeweight> turboAffinity;
        std::vector<index> neighboringCommunities;
        std::vector<double> communityVolumes;
        std::vector<double> nodeVolumes;
    };

}

#endif //LEIDEN_LOCALMOVINGALGORITHM_HPP
