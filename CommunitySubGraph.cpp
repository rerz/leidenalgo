#include <map>
#include <utility>
#include "CommunitySubGraph.hpp"

namespace NetworKit {

    CommunitySubGraph::CommunitySubGraph(Graph graph, std::map<node, node> subGraphToGraphMapping,
                                         std::map<node, node> graphToSubGraphMapping) : graph(std::move(graph)),
                                                                                        subGraphToGraphMapping(
                                                                                                std::move(
                                                                                                        subGraphToGraphMapping)),
                                                                                        graphToSubGraphMapping(
                                                                                                std::move(
                                                                                                        graphToSubGraphMapping)) {

    }

}